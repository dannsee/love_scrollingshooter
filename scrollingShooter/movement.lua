function canMoveUp(obj)
  return  obj.y > 0  
end
function moveUp(dt, obj)
  obj.y = obj.y  - ( obj.speed * dt ) 
end
function canMoveDown(obj)
  return  obj.y < ( love.graphics.getHeight()- obj.img:getHeight() )
end
function moveDown(dt, obj)
  obj.y = obj.y  + ( obj.speed * dt ) 
end 
function canMoveRight(dt, obj)
  return  obj.x < (love.graphics.getWidth() - obj.img:getWidth())
end
function moveRight(dt, obj)
  obj.x = obj.x + (obj.speed * dt)
end 
function canMoveLeft(obj)
  return obj.x > 0
end
function moveLeft(dt, obj)
  obj.x = obj.x - (obj.speed * dt ) 
end