function love.conf(t)
	t.title = "Scrolling shooter"
	t.version = "0.10.2"
	t.window.width = 480
	t.window.height = 700
	t.console = true
end

--Config file attributes http://www.love2d.org/wiki/Config_Files
