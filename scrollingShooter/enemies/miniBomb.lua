MiniBomb = {}
MiniBomb.__index = MiniBomb
miniBombImg = love.graphics.newImage('assets/miniBomb.png')
function MiniBomb:new (miniBomb)
    local self =  setmetatable({}, self)
    self.timerMax = 0.2
    self.timer = self.timerMax
    return self
end

function MiniBomb.update(self)
     return makeNewMiniBomb()
end

function makeNewMiniBomb()
    randomNumber = math.random(10, love.graphics.getWidth() -10) 
    newMiniBomb = {x = randomNumber, y = -10, img = love.graphics.newImage('assets/miniBomb.png')}
    return newMiniBomb
  
end

return MiniBomb