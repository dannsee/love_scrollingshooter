local Enemies = {}
Enemies.__index = Enemies

local MiniBomb = require('enemies/miniBomb')

function Enemies.new()
  local self = setmetatable({}, Enemies)
  self.miniBombTimerMax = 0.2
  self.miniBombTimer = self.miniBombTimerMax
  self._miniBombs = {}
  self._hits = {}
  return self
end

function Enemies.update(self, dt)
  updateMiniBombs(self, dt)
end

function Enemies.draw(self) 
    for j, enemy in ipairs(self._miniBombs) do 
      love.graphics.draw(enemy.img, enemy.x, enemy.y)
    end
end

function updateMiniBombs(self, dt)
    self.miniBombTimer = self.miniBombTimer - (1 * dt)
    if (self.miniBombTimer < 0 ) then
      self.miniBombTimer = self.miniBombTimerMax
      newMinibomb = MiniBomb:update()
      table.insert(self._miniBombs, newMinibomb)
    end   
    moveMiniBombs(self, dt)
end
function moveMiniBombs(self, dt)
  for i, miniBomb in ipairs(self._miniBombs) do
    miniBomb.y = miniBomb.y + (200 * dt) 
    if miniBomb.y > 850 then 
      table.remove(self._miniBombs, i)
    end
  end
end

return Enemies