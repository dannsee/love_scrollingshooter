--http://www.osmstudios.com/page/your-first-love2d-game-in-200-lines-part-3-of-3
 require 'player/player'
local Enemies = require('enemies/enemies')
hitSound = love.audio.newSource('assets/hit-sound.wav', 'static')
hashit = false
function love.load(arg)
  player = Player:new()
  enemies = Enemies.new()
end--___________________________________________________________________________________


function love.update(dt)
  player:update(dt, player)
  enemies:update(dt)

  checkCollisions()
  resetGame()
end--___________________________________________________________________________________


function love.draw()
  Player:draw()
  enemies:draw()
  drawResetgame()
  drawExplosions()
end--___________________________________________________________________________________


function resetGame()
  if not player.isAlive and love.keyboard.isDown('r') then 
    player = Player:new()
    enemies = Enemies.new()
  end
end--___________________________________________________________________________________


function drawResetgame()
  if player.isAlive then 
    love.graphics.draw(player.img, player.x, player.y)
  else
    love.graphics.print("Press r to restart", love.graphics:getWidth()/2 - 50 , love.graphics:getHeight() / 2 - 10 ) 
  end
end--___________________________________________________________________________________


function checkCollisions()
  for i, enemy in ipairs(enemies._miniBombs) do 
    for j, bullet in ipairs(player._bullets) do 
      if CheckCollision(enemy.x, enemy.y, enemy.img:getWidth(), enemy.img:getHeight(), bullet.x, bullet.y, bullet.img:getWidth(), bullet.img:getHeight()) then 
        table.remove(player._bullets, j) 
        table.remove(enemies._miniBombs, i)
        player.score = player.score + 1 
      end
    end
    
      
    if CheckCollision(enemy.x, enemy.y, enemy.img:getWidth(), enemy.img:getHeight(), player.x, player.y, player.img:getWidth(), player.img:getHeight()) and player.isAlive then 
      
        hit = {x = enemy.x, y = enemy.y, img = love.graphics.newImage('assets/bulletExplode.png'), framesToLive = 10}
          
        table.insert(enemies._hits, hit )
          
          
        table.remove(enemies._miniBombs, i)
        player.health = player.health - 1
        hitSound:play()
      
        if player.health == 0 then
          player.isAlive = false
        end
    end
  end
  
  for k, boom in ipairs(enemies._hits) do
    boom.framesToLive = boom.framesToLive - 1
  end
  
end--___________________________________________________________________________________

function drawExplosions()
  for i, boom in ipairs(enemies._hits) do
    love.graphics.draw(boom.img, boom.x, boom.y)
    if boom.framesToLive == 0 then 
      table.remove(enemies._hits, i)
    end
  end
end--___________________________________________________________________________________

-- Collision detection taken function from http://love2d.org/wiki/BoundingBox.lua
-- Returns true if two boxes overlap, false if they don't
-- x1,y1 are the left-top coords of the first box, while w1,h1 are its width and height
-- x2,y2,w2 & h2 are the same, but for the second box
function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end--___________________________________________________________________________________
