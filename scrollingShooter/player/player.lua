require 'movement'
require 'player/bullets'


Player = {} 
heart = love.graphics.newImage('assets/heart.png')

function Player:new (player)
    player = player or {
      x = 200, 
      y = 610, 
      speed = 200, 
      health = 5,
      img = love.graphics.newImage('assets/plane.png'), 
      isAlive = true, 
      score = 0, 
      canShoot = true,
      canShootTimerMax = 0.2,
      canShootTimer = canShootTimerMax
    } 
    self._bullets = {}
    setmetatable(player, self)
    self.__index = self
    return player
end

function Player:update(dt, player)
    updateBullets(dt, player)
    updateMovement(dt, player)
end

function Player:draw()
  drawBullets()
  drawHearts()
  drawScore()
end

function drawScore()
  love.graphics.print(tostring(player.score), 10,10 ) 
end

function drawHearts()
   heartPos = 5
   for i=1, player.health, 1 do
        love.graphics.draw(heart, heartPos, 30 )
        heartPos = heartPos + 30
    end
end



function updateMovement(dt, player)
    if love.keyboard.isDown('escape') then
    love.event.push('quit')
  end
  if love.keyboard.isDown('left', 'a') and love.keyboard.isDown('up', 'w') then
    if canMoveLeft(player) and canMoveUp(player) then 
      moveUp(dt, player)
    elseif canMoveUp(player) then
      moveUp(dt, player)
    end
  elseif love.keyboard.isDown('left', 'a') and love.keyboard.isDown('down', 's') then
    if canMoveLeft(player) and canMoveDown(player) then 
      moveLeft(dt, player)
      moveDown(dt, player)
    elseif canMoveDown(player) then
      moveDown(dt, player)
    end
  elseif love.keyboard.isDown('right', 'd') and love.keyboard.isDown('up', 'w') then
    if canMoveUp(player) and canMoveRight(dt, player) then 
      moveUp(dt, player)
      moveRight(dt, player)
    elseif canMoveUp(player) then
      moveUp(dt, player)
    end
  elseif love.keyboard.isDown('right', 'd') and love.keyboard.isDown('down', 's') then
    if canMoveRight(dt, player) and canMoveDown(player) then 
      moveDown(dt, player)
    elseif canMoveDown(player) then
      moveDown(dt, player)
    end
  elseif love.keyboard.isDown('left', 'a') then
    if canMoveLeft(player) then 
      moveLeft(dt, player)
    end
  elseif love.keyboard.isDown('up', 'w') then 
    if canMoveUp(player) then 
      moveUp(dt, player)
    end
  elseif love.keyboard.isDown('right', 'd') then 
    if canMoveRight(dt, player) then 
      moveRight(dt, player)
    end
  elseif love.keyboard.isDown('down', 's') then 
    if canMoveDown(player) then 
      moveDown(dt, player)
    end
  end
end



