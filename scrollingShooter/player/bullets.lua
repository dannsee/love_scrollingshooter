bulletImg = love.graphics.newImage('assets/bullet.png')
bulletShot = love.audio.newSource('assets/gun-sound.wav', 'static')
--Update Bullets
function updateBullets(dt, player)
  cShoot(dt, player)
  shoot(player)
  clear(dt)
end

function cShoot(dt, player)
  if player.canShootTimer ~= nil then
  player.canShootTimer = player.canShootTimer - (1 * dt)
  if player.canShootTimer < 0 and player.isAlive then
    player.canShoot = true
  end 
  end
end
  
function shoot(player)
  if love.keyboard.isDown('space', 'rctrl', 'lctrl', 'ctrl') and player.canShoot then 
    --create some bullets 
    newBullet = { x = player.x + (player.img:getWidth() / 2), y = player.y, img = bulletImg, explodeimg = bulletExplodeImg}
    table.insert(player._bullets, newBullet)
    player.canShoot = false
    player.canShootTimer = player.canShootTimerMax
    bulletShot:play()
  end
end

function clear(dt)
 for i, bullet in ipairs(player._bullets) do 
    bullet.y = bullet.y - (250 * dt )
     
    if bullet.y < 0 then 
      table.remove(player._bullets, i)
    end
  end
end
  
function drawBullets()
  for i, bullet in ipairs(player._bullets) do 
      love.graphics.draw(bullet.img, bullet.x, bullet.y) 
  end
end